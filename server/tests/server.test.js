const expect = require("expect");
const request = require("supertest");

const { app } = require("./../server");
var { Store } = require("./../models/store");

describe("POST request", () => {
  //Clearing the test database before running tests
  before(async () => {
    await Store.deleteMany({});
  });

  it("should return an error code when an empty object is passed in the request", done => {
    request(app)
      .post("/object")
      .send({})
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it("should create a new record in the DB is a new key is passed", done => {
    request(app)
      .post("/object")
      .send({ mykey: "value" })
      .expect(200)
      .expect(res => {
        expect(res.body.key).toBe("mykey");
        expect(res.body.value).toBe("value");
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        Store.findById("mykey")
          .then(doc => {
            expect(doc._id).toBe("mykey");
            expect(typeof doc.current).toBe("object");
            expect(doc.current.value).toBe("value");
            done();
          })
          .catch(e => done(e));
      });
  });

  it("should update the value if an existing key is passed and save the previous value in prev array", done => {
    request(app)
      .post("/object")
      .send({ mykey: "value2" })
      .expect(200)
      .expect(res => {
        expect(res.body.key).toBe("mykey");
        expect(res.body.value).toBe("value2");
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        var key = "mykey";
        var value = "value2";

        Store.findById(key)
          .then(doc => {
            expect(doc._id).toBe(key);
            expect(doc.current.value).toBe(value);
            expect(doc.prev.length).toBe(1);
            expect(doc.prev[0].value).toBe("value");
            done();
          })
          .catch(e => done(e));
      });
  });
});

describe("GET requests", () => {
  //Seeding the database with some test data
  before(async () => {
    //Getting the UNIX timestamp in *seconds*. The default is milliseconds
    var timestamp = Math.round(new Date().getTime() / 1000);
    //Adding a new key
    await Store.updateStore("gettest", "value1", timestamp);
    //Adding more versions to the key above
    await Store.updateStore("gettest", "value2", timestamp);
    await Store.updateStore("gettest", "value3", timestamp);
    await Store.updateStore("gettest", "value4", timestamp);
  });

  it("should return a 400 when the key in the request does not exist", done => {
    request(app)
      .get("/object/key")
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it("should return the latest value of a key when no timestamp is passed", done => {
    request(app)
      .get("/object/gettest")
      .expect(200)
      .expect(res => {
        expect(res.value).toBe("value4");
      })
      .end(() => done());
  });

  it("should return the value related to the timestamp if a correct timestamp is passed", done => {
    //Storing a valid timestamp from the db for test below
    var testTimestamp;
    Store.findById("gettest").then(doc => {
      testTimestamp = doc.prev[1].timestamp;
    });

    request(app)
      .get(`/object/gettest?timestamp=${testTimestamp}`)
      .expect(200)
      .expect(res => {
        expect(res.value).toBe("value2");
      })
      .end(() => done());
  });

  it("should return 400 if the timestamp is dated before the key was generated for the first time", done => {
    request(app)
      .get("/object/gettest?timestamp=123")
      .expect(400)
      .end(() => done());
  });

  it("should return the latest value if current timestamp is provided after the value was recently updated", done => {
    var currentTimestamp = Math.round(new Date().getTime() / 1000);
    request(app)
      .get(`/object/gettest?timestamp=${currentTimestamp}`)
      .expect(200)
      .expect(res => {
        expect(res.value).toBe("value4");
      })
      .end(() => done());
  });
});
