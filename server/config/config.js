//Production environment is not set explicitly because it will be set by heroku

var env = process.env.NODE_ENV || 'development';

if(env === 'development' ||  env ==='test') {
    var config = require('./config.json');
    var envConfig = config[env];//Written in bracket notation rather than dot notation because env is a variable and not a direct property
    
    Object.keys(envConfig).forEach((key) => {
        process.env[key] = envConfig[key]; 
    });
}