var mongoose = require("mongoose");
const _ = require("lodash");

var StoreSchema = new mongoose.Schema({
  _id: {
    type: String,
    required: true
  },
  current: {
    timestamp: {
      type: Number,
      required: true
    },
    value: {
      type: Object,
      required: true
    }
  },
  prev: [
    {
      timestamp: {
        type: Number
      },
      value: {
        type: Object
      }
    }
  ]
});

StoreSchema.statics.updateStore = async function(key, value, timestamp) {
  var Store = this;

  //If a proper JSON object isn't passed with the POST request
  if (!key || !value || _.isEmpty(value)) {
    return Promise.reject(
      'Please send a proper JSON object with the POST request. Example {"key": "value"}'
    );
  }

  var existingDoc = await Store.findById(key);

  if (!existingDoc) {
    var store = new Store({
      _id: key,
      current: {
        timestamp,
        value
      },
      prev: []
    });

    return store.save().then(doc => {
      return doc;
    });
  } else {
    /*
     * This can be added if we want to avoid updating the current value,
     * if the value passed in the request is same as the current value
     * in the database.
     */
    // if (existingDoc.current.value == value) {
    //   return Promise.reject("That's the current value");
    // }

    existingDoc.prev.push(existingDoc.current);
    existingDoc.current = { timestamp, value };
    existingDoc.set({ existingDoc });
    return existingDoc.save().then(doc => doc);
  }
};

StoreSchema.statics.getTimestampedValue = async function(key, timestamp) {
  var doc = await Store.findById(key);
  if (!doc) {
    return Promise.reject("Key does not exist");
  }

  //  If the timestamp passed in the request in
  //  greater than or equal to the current
  //  value's timestamp
  if (doc.current.timestamp <= timestamp) {
    return Promise.resolve(doc.current);
  }

  var record = doc.prev
    .filter(obj => {
      return obj.timestamp <= timestamp;
    })
    .pop();

  // If the timestamp provided is dated
  // before the key was generated for the first time.
  if (_.isEmpty(record)) {
    return Promise.reject("There is no value for the requested timestamp");
  }

  return Promise.resolve(record);
};

var Store = mongoose.model("Store", StoreSchema);

module.exports = {
  Store
};
