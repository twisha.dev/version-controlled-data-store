require("./config/config");

const express = require("express");
const bodyParser = require("body-parser");
const _ = require("lodash");

require("./db/mongoose");
var { Store } = require("./models/store");

var app = express();
var port = process.env.PORT;

app.use(bodyParser.json());

app.post("/object", async (req, res) => {
  var key = Object.keys(req.body)[0];
  var value = req.body[key];
  //Getting the UNIX timestamp in *seconds*. The default is milliseconds
  var timestamp = Math.round(new Date().getTime() / 1000);

  try {
    var doc = await Store.updateStore(key, value, timestamp);
    var sendRecord = {
      key: doc._id,
      value: doc.current.value,
      timestamp: doc.current.timestamp
    };
    res.send(sendRecord);
  } catch (e) {
    res.status(400).send(e);
  }
});

app.get("/object/:key", async (req, res) => {
  var key = req.params.key;

  //If someone enters a URL parameter other than timestamp
  if (!_.isEmpty(req.query) && !req.query.timestamp) {
    res
      .status(400)
      .send('Please provide a url query parameter named "timestamp"');
  }

  var timestamp = req.query.timestamp;

  if (!timestamp) {
    Store.findById(key).then(
      doc => {
        if (!doc) {
          return res.status(400).send("Key does not exist.");
        }
        res.send({ value: doc.current.value });
      },
      e => {
        res.status(400).send();
      }
    );
  } else {
    try {
      var doc = await Store.getTimestampedValue(key, timestamp);
      res.status(200).send({ value: doc.value });
    } catch (err) {
      res.status(400).end(err);
    }
  }
});

app.listen(port, () => {
  console.log(`Started on port ${port}`);
});

module.exports = { app };
