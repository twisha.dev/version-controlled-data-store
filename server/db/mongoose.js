var mongoose = require('mongoose');
var MongoUrl = process.env.MONGODB_URI;

mongoose.Promise = global.Promise; //Generally mongoose uses callbacks so we need to configure this to make sure it uses promises.
mongoose.connect(MongoUrl, { useNewUrlParser: true });

module.exports = {
    mongoose
};