# Version Controlled Data Store

This is an API to manage a version controlled key-value store. It is scripted in NodeJS and uses MongoDB as its database. The API -

1. Accepts a key(string) and value(some json blob/string) {"key" : "value"} and stores them. If an existing key is sent, the value is updated

2. Accepts a key and returns the corresponding latest value

3. When given a key AND a timestamp, returns whatever the value of the key at the time was or the value that was latest before that time. For example, if the vaue was updated at 6PM and then again at 6.05 PM, if given a timestamp for 6.03 PM it will return the value of the key at 6 PM as that was the latest value at that time.

Reasoning behind the choice of schema and alternative possibilities are described at the end.

Live URL - https://versiondata.herokuapp.com/

**Note**- As it is hosted on Heroku's free tier, the app sleeps after 30 mins of inactivity so the first request might take some time to respond.

## API Endpoints

Note- All UNIX timestamps are in seconds.

#### 1) POST /object

Sample Request

```
https://versiondata.herokuapp.com/object

{"key": "value"}
```

Sample Response

```
{
    "key": "key",
    "value": "value",
    "timestamp": timestamp (will show the UNIX timestamp of when the request was made)
}
```

If a POST request is made with an existing key, the value is updated and a similar response as above will be returned with the updated value and timestamp.

#### 2) GET /object/key

Sample Request

```
https://versiondata.herokuapp.com/object/key

```

Sample Response

```
{
    "value": "value"
}
```

Gets the most current value related to the key.

#### 3) GET /object/key?timestamp=someTimestamp

Timestamp should be in seconds to get a proper result.
Sample Request

```
https://versiondata.herokuapp.com/object/key?timestamp=1548408604

```

Sample Response

```
{
    "value": "val1"
}
```

Gets the value of the key at that time or the last one before that time

## Run this Locally

Make sure you have node and npm installed on your system.

1. To run this app in your local environment clone this repository and run

```
npm install
```

2. After the installation is complete add a config.json file to the path server/config. This file will contain configurations for your local port and MongoDB for both test and development environments.

```
{
    "test" : {
        "PORT": 3000,
        "MONGODB_URI": "mongodb://127.0.0.1:27017/DBTest"
    },
    "development" : {
        "PORT": 3000,
        "MONGODB_URI": "mongodb://127.0.0.1:27017/DBMain"
    }
}
```

3. Run

```
node server/server.js

```

You can test the API locally using [Postman](https://www.getpostman.com/) or any other method you prefer for testing APIs.

## Tests

Run the following command in your console after entering the project directory. Please remember to add the config.json file as mentioned above or the tests won't run.

```
npm test

```

## Explanation for Schema Design

This app uses MongoDB's document-oriented design to store different versions of the value i.e it stores all the versions in a single document. 

This app stores all the versions of a particular key in the same document (including the current version). Advantages of this design are listed below.

* *atomic* updates - the document is updated atomically, i.e., setting the new value and archiving the previous version is done in a single transaction,

* small updates - instead of writing a full document only field-level updates are made.

*Alternative design: separate collections can be used for current and previous data. This helps when document sizes become very large, however this method also increases number of database transactions.*
